function slideToPrimaryContent(action) 
{
	changePrimaryContentLeftMarginTo(action, '0%');
}

function slideToSecondaryContent(action) 
{
	changePrimaryContentLeftMarginTo(action, '-100%');
}

function changePrimaryContentLeftMarginTo(action, percentage) 
{
	$(`#${OVERLAY_ID}`).css('display', 'block');
	$(`#${PRIMARY_CONTENT_ID}`).css('marginLeft', percentage);

	if (action != null) 
	{
		$(`#${PRIMARY_CONTENT_ID}`).one("transitionend", function () 
		{
			action();
		});
	}
}

function instantlySlideToMainView() 
{
	setContentMarginLeftTo(0);
}

function instantlySlideToSecondaryView() 
{
	setContentMarginLeftTo(-100);
}

function setContentMarginLeftTo(value) 
{
	var mainContent = document.getElementById(PRIMARY_CONTENT_ID);
	var otherContent = document.getElementById(SECONDARY_CONTENT_ID);

	// No transition animation until the margin left is at the right value
	mainContent.classList.add("notransition");
	otherContent.classList.add("notransition");

	mainContent.style.marginLeft = value + "%";;

	// Force update
	mainContent.offsetHeight;

	mainContent.classList.remove("notransition");
	otherContent.classList.remove("notransition");
}