var LIST_VIEW_HTML = "projectList";
var PROJECT_VIEW_HTML = "projectItem";
var PROJECT_TITLE_HTML = "projectTitle";
var LIST_CONTAINER_ID = "list";


async function showList(name) 
{
	showPrimaryPage(name, loadList);
}

async function loadList(containerID, jsonName, cancellationToken) 
{
	return new Promise((onResolve) => 
	{
		var container = $(`#${containerID}`);
		$.getJSON(`Static-Data/${jsonName}.json`, async function(json) 
		{
			if (cancellationToken.isCancellationRequested)
				return;
			
			container.html("");

			for(var i = 0; i < json.list.length; i++) 
			{
				if (cancellationToken.isCancellationRequested)
					return;

				var projectGroup = json.list[i];
				await loadProject(container, projectGroup, jsonName, cancellationToken);
			}
			
			await loadSpacer(container);

			onResolve();
		});
	});
	
}

async function loadProject(container, projectGroup, listJSON, cancellationToken)
{
	await loadProjectTitle(container, projectGroup.title, cancellationToken);
	await loadProjectGroup(container, projectGroup.projects, listJSON, cancellationToken);
}

function loadProjectTitle(container, title, cancellationToken)
{
	return new Promise((onResolve, onError) =>
	{
		$.get(`Pages/${PROJECT_TITLE_HTML}.html`, function(data)
		{
			if (cancellationToken.isCancellationRequested)
			{
				onError();
				return;
			}

			var projectItem = $(data);
			projectItem.text(title);
			projectItem.appendTo(container);

			onResolve();
		});
	});
}

function loadProjectGroup(container, projects, listJSON, cancellationToken)
{
	return new Promise((onResolve, onError) =>
	{
		$.get(`Pages/${LIST_VIEW_HTML}.html`, async function(data)
		{
			var projectContainer = $(data);
			for(var i = 0; i < projects.length; i++) 
			{
				if (cancellationToken.isCancellationRequested)
				{
					onError();
					return;
				}

				var project = projects[i];
				await loadProjectItem(projectContainer, project, listJSON, cancellationToken);
			}
			
			projectContainer.appendTo(container);
			onResolve();
		});
	});
}

function loadProjectItem(container, project, listJSON, cancellationToken)
{
	return new Promise((onResolve, onError) =>
	{
		$.get(`Pages/${PROJECT_VIEW_HTML}.html`, function(data)
		{
			if (cancellationToken.isCancellationRequested)
			{
				onError();
				return;
			}

			var projectItem = $(data);
			projectItem.find(".list-image").attr("src", `Images/${project.image}`);
			projectItem.find(".list-title").text(project.title);
			projectItem.find(".list-sub-title").text(project.subTitle);
			projectItem.find(".list-description").text(project.description);
			projectItem.appendTo(container);

			(function(link) {
				projectItem.click(function() { showItem(listJSON, link) });
			})(project.clickLink);

			onResolve();
		});
	});
}

