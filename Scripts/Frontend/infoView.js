function showAbout() 
{
	showPrimaryPage("About", loadAbout);
}

function showContact() 
{
	showPrimaryPage("Contact", loadContact);
}

function loadAbout(containerID, contentName, cancellationToken) 
{
	loadHtml(containerID, aboutView, cancellationToken);
}

function loadContact(containerID, contentName, cancellationToken) 
{
	loadHtml(containerID, contactView, cancellationToken);
}

function loadHtml(containerID, contentName, cancellationToken)
{
	var container = $(`#${containerID}`);
	$.get(`Pages/${contentName}.html`, function(data)
	{
		container.html("");
		if (cancellationToken.isCancellationRequested)
			return;

		$(data).appendTo(container);
	});
}