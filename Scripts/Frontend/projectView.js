var PRIMARY_CONTENT_ID = "content-primary";
var SECONDARY_CONTENT_ID = "content-secondary";
var contentTitleName = "content-title";
var contentDescriptionName = "content-description";
var contentTechnologyName = "content-technology";

var currentSlide = 0;
var loadingFunctions = [];

// Those two variables are used to synchronize between loading and transitions
var finishedLoad = false;
var finishedTransition = false;

function loadImage(button, index, src) 
{
	if (index == currentSlide)
		return;

	var imageShowcase = document.getElementById("image-showcase");
	var videoShowcase = document.getElementById("video-showcase");

	$('.yt_player_iframe').each(function () {
		this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*')
	});

	$('.project-image-highlighted').removeClass("project-image-highlighted");

	button.addClass("project-image-highlighted");

	imageShowcase.style.display = 'block';
	videoShowcase.style.display = 'none';

	imageShowcase.src = src;

	currentSlide = index;
}

function loadVideo(button, index, src) 
{
	if (index == currentSlide)
		return;

	var imageShowcase = document.getElementById("image-showcase");
	var videoShowcase = document.getElementById("video-showcase");

	$('.project-image-highlighted').removeClass("project-image-highlighted");

	button.addClass("project-image-highlighted");

	imageShowcase.style.display = 'none';
	videoShowcase.style.display = 'block';

	videoShowcase.src = src;

	currentSlide = index;
}

function prev() 
{
	if (currentSlide == 0)
		return;

	loadingFunctions[currentSlide - 1]();
}

function next() 
{
	if (currentSlide == loadingFunctions.length - 1)
		return;

	loadingFunctions[currentSlide + 1]();
}

async function goBack() 
{
	showList(currentList);
}

function showItem(listName, name, fromDropdown = false) 
{
	var isInMobile = isInMobileMode();

	if (currentView == name) {
		return;
	}

	if (currentView != listName && currentView != name) {
		loadViewInSecondaryTab(name, loadItem, finishLoadingVideo);
	}
	else if (listName == currentList) {
		slideToSecondaryContent(onTransitionEnd);
		loadItem(SECONDARY_CONTENT_ID, name, function () { createScrollbar(SECONDARY_CONTENT_ID); });
		currentView = name;
	}
	else {
		loadViewInSecondaryTab(name, loadItem, finishLoadingVideo);
	}

	currentList = listName;
}


function loadItem(contentName, name, onLoadFinished) 
{
	$("#" + SECONDARY_CONTENT_ID).load("Pages/project" + ".html", function () {
		$.getJSON("Static-Data/" + currentList + "/" + name + ".json", function (json) {
			var title = json.title;
			var listName = json.listName;
			var videos = json.videos;
			var images = json.images;
			var imageFolder = json.imageFolder;
			var descriptions = json.descriptions;
			var technologies = json.technologies;
			var buttonText = json.buttonText;
			var buttonLink = json.buttonLink;

			currentImageFolder = imageFolder;
			
			$(`#${contentTitleName}`).text(title);

			var globalIndex = 0;
			var quickLinksDiv = document.getElementById("quick-links");

			currentSlide = 0;
			loadingFunctions = [];

			$('.back-button').text("Back");

			for (var index in videos) {
				var videoTitle = videos[index].title;
				var videoLink = videos[index].link;

				var videoButton = $('<button />')
					.text(videoTitle)
					.attr('class', 'custom-button expand-on-hover text-font-medium text-color-1')
					.attr('style', 'margin-right: 4px')
					.appendTo(quickLinksDiv);

				(function (button, index, videoLink) {
					var number = parseInt(index);
					videoButton.click(function () { loadVideo(button, number, videoLink); });
					loadingFunctions.push(function () { loadVideo(button, number, videoLink); });
				})(videoButton, index, videoLink);

				globalIndex++;
			}

			for (var index in images) {
				var image = images[index];

				var imageLink = $('<button />')
					.text(index)
					.attr('class', 'image-button expand-on-hover background-color-2 text-font-medium text-color-1')
					.appendTo(quickLinksDiv);

				(function (button, index, globalIndex, image) {
					var number = parseInt(index);
					imageLink.click(function () { loadImage(button, globalIndex, imageFolder + image); });
					loadingFunctions.push(function () { loadImage(button, globalIndex, imageFolder + image); });
				})(imageLink, index, globalIndex, image);

				globalIndex++;
			}

			var contentTechnologyDiv = document.getElementById(contentTechnologyName);
			for (var index in technologies) {
				var technology = technologies[index];

				var technologyDiv = $('<span />')
					.attr('class', "paragraph center-all");

				var technologyText = $('<span />')
					.text(technology)
					.appendTo(technologyDiv);

				technologyDiv.appendTo(contentTechnologyDiv);
			}

			var contentDescriptionDiv = document.getElementById(contentDescriptionName);
			for (var index in descriptions) {
				var description = descriptions[index];

				var technologyText = $('<span />')
					.text(description)
					.attr('class', 'paragraph')
					.appendTo(contentDescriptionDiv);
			}

			currentSlide = -1;

			$('#project-button').text(buttonText);
			$('#project-button').attr("href", buttonLink);

			onLoadFinished();

			finishedLoad = true;

			if (finishedTransition && finishedLoad) {
				finishLoadingVideo();
			}
		});
	});
}

function onTransitionEnd() 
{
	finishedTransition = true;

	if (finishedTransition && finishedLoad) {
		finishLoadingVideo();
	}
}


function finishLoadingVideo() 
{
	var container = document.getElementsByClassName('video-container');

	var youtubeVideo = $('<iframe />')
		.attr('id', 'video-showcase')
		.attr('class', 'yt_player_iframe')
		.attr('allowscriptaccess', 'always')
		.attr('allowfullscreen', '1')
		.appendTo(container);

	loadingFunctions[0]();

	finishedTransition = false;
	finishedLoad = false;
}