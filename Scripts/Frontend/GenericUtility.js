function loadSpacer(container)
{
	return new Promise((onResolve) =>
	{
		$.get("Pages/spacer.html", function(data)
		{
			var spacer = $(data);
			spacer.appendTo(container);
			
			onResolve();
		});
	});
}

function stopYoutubeVideos() 
{
	$('.yt_player_iframe').each(function () 
	{
		this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*')
	});
}

function isInMobileMode() 
{
	var matchesWidth = window.matchMedia("(max-width: 768px)");
	var matchesHeight = window.matchMedia("(max-height: 700px)");
	return matchesWidth.matches || matchesHeight.matches;
}