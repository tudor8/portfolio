// Constants
var OVERLAY_ID = "overlay";
var projectView = "project";
var aboutView = "about";
var educationView = "education";
var contactView = "contact";
var resumeView = "resume";

// Variables
var currentList = "";
var currentView = "";
var pageLoadingCancellationSource = new Token();

// Content --------
function setAllContentsToHeight(height) {
	$('.content').css('height', height + '%');
}

function minimizeContents(action) {
	$(`#${OVERLAY_ID}`).css('display', 'none');

	var content = document.getElementById(PRIMARY_CONTENT_ID);
	if (getComputedStyle(content, null).height != '0px') 
	{
		stopYoutubeVideos();

		setAllContentsToHeight(0);

		return new Promise((onResolve) => 
		{
			$('#' + PRIMARY_CONTENT_ID).one("transitionend", function (event) 
			{
				onResolve();
			})
		});
	}

	return Promise.resolve();
};

function maximizeContents() 
{
	$(`#${OVERLAY_ID}`).css('display', 'block');

	setAllContentsToHeight(100);

	return new Promise((onResolve) => 
	{
		$('#' + PRIMARY_CONTENT_ID).one("transitionend", function (event) 
		{
			onResolve();
		})
	});
};

// Scrollbars must be created after the content is created, otherwise they will not have the right height set
function createScrollbar(name) 
{
	if (isInMobileMode()) 
		return;
	
	var content = document.getElementById(name)

	destroyScrollbars(name);

	OverlayScrollbars(content, { className: "os-theme-light", paddingAbsolute: true });
}

function destroyScrollbars(name) 
{
	var content = document.getElementById(name)

	var instance = OverlayScrollbars(content);
	if (instance !== undefined) {
		instance.destroy();
	}
}

function showPrimaryPage(pageName, loadContentFunction) 
{
	if (currentList == pageName && currentView != "" && currentList != currentView) {
		slideToPrimaryContent();
		currentView = pageName;
	}
	else {
		loadViewInMainTab(pageName, loadContentFunction, function () { }, pageLoadingCancellationSource);
	}

	currentList = pageName;
}

function loadViewInMainTab(name, loadContentFunction, onAnimationEnd) 
{
	pageLoadingCancellationSource.cancel();
	pageLoadingCancellationSource = new Token();
	loadView(name, loadContentFunction, onAnimationEnd, PRIMARY_CONTENT_ID, instantlySlideToMainView, pageLoadingCancellationSource);
}

function loadViewInSecondaryTab(name, loadFunction, onAnimationEnd) 
{
	loadView(name, loadFunction, onAnimationEnd, SECONDARY_CONTENT_ID, instantlySlideToSecondaryView);
}

async function loadView(contentName, loadFunction, onAnimationEnd, containerID, switchAction, token) 
{
	// Same view -> Hide current view
	if (currentView == contentName) 
	{
		unhighlightAllTabButtons();

		await minimizeContents();
		currentView = "";
		currentList = "";
	}
	// Different view -> minimize current view and then maximize the new content
	else 
	{
		currentView = contentName;
		await minimizeContents();
		destroyScrollbars(containerID);
		await loadFunction(containerID, contentName, token);
		switchAction();
		await maximizeContents();
		onAnimationEnd();
		createScrollbar(containerID);
	}
}

async function hideContent() 
{
	await minimizeContents();
	
	if (isInMobileMode())
		return;

	currentView = '';
	currentList = '';

	unhighlightAllTabButtons();
}