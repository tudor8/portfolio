function unhighlightAllTabButtons() 
{
	$(".button-highlighted").removeClass("button-highlighted");
}

function onTabButtonClick(button, action) 
{
	unhighlightAllTabButtons();

	$(button).addClass("button-highlighted");

	if (action != null) 
		action();
}

function onDropdownButtonClick(button, action) 
{
	unhighlightAllTabButtons();

	button.parentNode.style.height = '0%';

	$(button).one("transitionend", function (event) { button.parentNode.style.height = ''; })

	$(button.parentNode.parentNode.parentNode.getElementsByTagName('button')[0]).addClass("button-highlighted");

	if (action != null) 
		action();
}