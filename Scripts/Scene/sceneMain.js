var canvas = document.getElementById('container');
var w = container.clientWidth;
var h = container.clientHeight;		

//container.appendChild(renderer.domElement);

//var island = loadIsland();
//island.position.set(0, -10, 0);
//scene.add(island);

const sceneManager = new SceneManager(canvas);

var onSceneResize = function() {
	canvas.style.width  = '100vw';
	canvas.style.height = '100vh';

	canvas.width  = canvas.offsetWidth;
	canvas.height = canvas.offsetHeight;

	sceneManager.onSceneResize();
}

var gameLoop = function() {
	requestAnimationFrame(gameLoop);
	sceneManager.update();
}

window.addEventListener('resize', onSceneResize);
onSceneResize();

gameLoop();