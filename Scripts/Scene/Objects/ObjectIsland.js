function Island(scene, camera) {
	var textures = {};

	var group = new THREE.Group();

	var mobileMode = isInMobileMode();

	if(!mobileMode) {
		$('#percentage-done').parent().css("display", "block");
	}

	var island = loadIsland(mobileMode);
	island.position.set(0, -10, 0);

	//const marker1 = new Marker( new THREE.Vector3(   0, 0, 0 ), scene, camera);
	//const marker2 = new Marker( new THREE.Vector3( 7, 0, 7 ), scene, camera);

	//group.add(island);

	//scene.add(group);

	this.update = function(scene) {
		island.rotation.y += 0.001;
	}

	this.islandObj = function() {
		return island;
	}

	function isInMobileMode() {
		var x = window.matchMedia("(max-width: 768px)");
		return x.matches;
	}

	function getTexture(name) {
		if(name in textures) {
			return textures[name];
		}
		else {
			var texture  = new THREE.TextureLoader().load( 'Data/' + name);
			texture.minFilter = THREE.LinearFilter;
			textures[name] = texture;
			return texture;
		}
	}


	function loadIsland(mobileMode) {	
		var treeCount = 0;
		var island = new THREE.Group();
		$.getJSON("Data/data.json", function(json) {
		    for(var chunkIndex in json.topChunks) {
		    	var chunk = json.topChunks[chunkIndex];

		    	var object = loadMesh(chunk);

				island.add(object);
		    }

		    for(var chunkIndex in json.botChunks) {
		    	var chunk = json.botChunks[chunkIndex];

		    	var object = loadMesh(chunk);

				island.add(object);
		    }

		    island.position.set(0, -10, 0);

		    scene.add(island);

		    treeCount = json.numberOfTrees;

		    $('#percentage-done').text(Math.floor((3) / (3 + treeCount) * 100));

		    if(!mobileMode) {
			    loadTrees(treeCount);
			}
			else {
				$('#loader').css("display", "none");
			}
		});

		return island;
	}

	function loadTrees(count) {
		tree(1, count);
	}

	function tree(index, limit) {
		$('#percentage-done').text(Math.floor((3 + index) / (3 + limit) * 100));

		if(index > limit) {
			$('#loader').css("display", "none");
			return;
		}

		$.getJSON("Data/Tree" + index + ".json", function(json) {
		    for(var meshIndex in json.meshes) {
		    	var treeMesh = json.meshes[meshIndex];

		    	var object = loadMesh(treeMesh);

				island.add(object);
		    }

		    tree(index + 1, limit);
		}); 
	}

	function loadMesh(data) {
		var geom = new THREE.Geometry(); 

		var texture  = getTexture(data.textureName);
		var material = new THREE.MeshPhongMaterial({ map : texture })

		for(var i in data.vertices) {
			var vertex = data.vertices[i];
			geom.vertices.push(new THREE.Vector3(vertex.x, vertex.y, vertex.z));
		}

		for(i = 0; i < data.triangles.length; i+=3) { 
			geom.faceVertexUvs[0].push([
				new THREE.Vector2(data.uvs[data.triangles[i    ]].x, data.uvs[data.triangles[i    ]].y),
				new THREE.Vector2(data.uvs[data.triangles[i + 1]].x, data.uvs[data.triangles[i + 1]].y),
				new THREE.Vector2(data.uvs[data.triangles[i + 2]].x, data.uvs[data.triangles[i + 2]].y)
			]);
		}

		for(i = 0; i < data.triangles.length; i+=3) { 
			geom.faces.push( new THREE.Face3(data.triangles[i], data.triangles[i + 1], data.triangles[i + 2]) );
		}

		object = new THREE.Mesh( geom, new THREE.MeshPhongMaterial({
			    map: texture,
			    flatShading: true,
			    shininess: 1
			})
		);

		object.position.x = data.position.x;
		object.position.y = data.position.y;
		object.position.z = data.position.z;

		return object;
	}
}