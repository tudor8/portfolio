function Marker(position, scene, camera) {
	// var geometry = new THREE.BoxGeometry( 1, 1, 1 );
	// var material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
	
	// var cubeA = new THREE.Mesh( geometry, material );

	// var cubeB = new THREE.Mesh( geometry, material );
	// cubeB.position.set(0, 2, 0);

	// var geometry = new THREE.Geometry();
	// var x = 0;
	// var y = 0;
	// var z = 0;
	// var radius = 1;
	// geometry.vertices.push( new THREE.Vector3( x+0, y+radius, z ) );
	// geometry.vertices.push( new THREE.Vector3( x+radius*0.866, y+radius*0.5, z ) );
	// geometry.vertices.push( new THREE.Vector3( x+radius*0.866, y-radius*0.5, z ) );
	// geometry.vertices.push( new THREE.Vector3( x+0, y-radius, z ) );
	// geometry.vertices.push( new THREE.Vector3( x-radius*0.866, y-radius*0.5, z ) );
	// geometry.vertices.push( new THREE.Vector3( x-radius*0.866, y+radius*0.5, z ) );
	// geometry.vertices.push( new THREE.Vector3( x+0, y+radius, z ) );

	// var line = new THREE.Line( geometry, material );

	// var group = new THREE.Group();
	// group.add( cubeA );
	// group.add( cubeB );
	// group.add(line);
	
	// console.log(position);
	// group.position.set(position.x, position.y, position.z);
	
	// scene.add(group);

	var texture  = new THREE.TextureLoader().load( 'Images/chungus.png');
	texture.minFilter = THREE.LinearFilter;
	var material = new THREE.SpriteMaterial( { map: texture, color: 0xffffff, fog: true } );
	var sprite = new THREE.Sprite( material );
	sprite.position.set(0, 11, 0);
	sprite.scale.set(3, 3, 1);

	var hexagon = new Hexagon(1, 0.1);

	material = new THREE.MeshPhongMaterial();
	material.transparent = true;
	material.opacity = 0.3;
	var geometry = new THREE.ConeBufferGeometry( 1, 10, 6 );
	var cube = new THREE.Mesh( geometry, material );
	cube.position.set(0, 5, 0);

	var group = new THREE.Group();
	group.add( sprite );
	group.add( hexagon );
	group.add( cube );

	group.position.set(position.x, position.y, position.z);

	scene.add(group);

	this.update = function() {
		//cubeB.lookAt(camera.position);
	}

	this.getObj = function() {
		return group;
	}
}