function Hexagon(radius, height) {
	var shape = new THREE.Shape();
	var goldenRule = 0.866;
	shape.moveTo( 0, radius );
	shape.lineTo( radius * goldenRule, radius * 0.5 );
	shape.lineTo( radius * goldenRule, -radius * 0.5 );
	shape.lineTo( 0, -radius );
	shape.lineTo( -radius * goldenRule, -radius * 0.5 );
	shape.lineTo( -radius * goldenRule, radius * 0.5 );
	shape.lineTo( 0, radius );

	var extrudeSettings = {
		steps: 2,
		depth: height,
		bevelEnabled: false,
		bevelThickness: 1,
		bevelSize: 1,
		bevelSegments: 0
	};

	var geometry = new THREE.ExtrudeGeometry( shape, extrudeSettings );

	var material = new THREE.MeshPhongMaterial();
	var mesh = new THREE.Mesh( geometry, material ) ;
	mesh.rotateX(Math.PI / 2);
	mesh.position.set(0, height / 2, 0);
	
	return mesh;
} 