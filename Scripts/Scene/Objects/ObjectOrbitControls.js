function OrbitControls(scene, camera, renderer) {
	var controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.minPolarAngle = 0.35 * Math.PI; // radians
	controls.maxPolarAngle = 0.4 * Math.PI;
	controls.enableDamping = true;
	controls.dampingFactor = 0.07;
	controls.rotateSpeed   = 0.1;
	controls.enablePan = false;
	controls.minDistance = 10;
	controls.maxDistance = 350;
	controls.target.set(0, 0, 0)
	controls.update();

	this.update = function() {
		controls.update();
	}

	return controls;
}
