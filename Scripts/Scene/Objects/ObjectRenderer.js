function ObjectRenderer(scene, canvas, width, height) {
	const renderer = new THREE.WebGLRenderer({ canvas: canvas, antialias: true, alpha: true }); 
    const DPR = (window.devicePixelRatio) ? window.devicePixelRatio : 1;
    renderer.setPixelRatio(DPR);
    renderer.setSize(width, height);

    renderer.gammaInput = true;
    renderer.gammaOutput = true; 

    this.update = function() {
		
	}

    return renderer;
}