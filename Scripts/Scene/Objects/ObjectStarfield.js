function Starfield(scene) {
	var starsGeometry = new THREE.Geometry();

	for ( var i = 0; i < 10000; i ++ ) {
		var distance = 1000;
		var star = new THREE.Vector3();
		star.x = THREE.Math.randFloatSpread( distance );
		star.y = THREE.Math.randFloatSpread( distance );
		star.z = THREE.Math.randFloatSpread( distance );

		var distanceFromStart = vectorDistance(star, new THREE.Vector3());
		if(distanceFromStart > 300 && distanceFromStart < 900) {
			starsGeometry.vertices.push( star );
		}
	}

	var starsMaterial = new THREE.PointsMaterial( { color: 0x888888 } );

	var starField = new THREE.Points( starsGeometry, starsMaterial );

	scene.add(starField);

	this.update = function() {
		
	}
}