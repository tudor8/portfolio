function SceneManager(canvas) {
	var scene  = new THREE.Scene();

	const renderer = new ObjectRenderer(scene, canvas, canvas.width, canvas.height);
	const camera = new Camera (scene, isInMobileMode());
	const orbitControls = new OrbitControls(scene, camera, renderer);

	const island = new Island(scene, camera);
	
	const objs = createObjs(scene);
	
	function createObjs(scene) {
		const objs = [
			orbitControls,
			new AmbientLight    (scene),
			new DirectionalLight(scene),
			new Starfield       (scene),
			island
		];

		return objs;
	}

	function isInMobileMode() {
		var x = window.matchMedia("(max-width: 768px)");
		return x.matches;
	}

	this.update = function() {
		for(var i = 0; i < objs.length; i++) {
			objs[i].update(scene);
		}

		renderer.render(scene, camera);
	}

	this.onSceneResize = function() {
		renderer.setSize(canvas.width, canvas.height);
		renderer.setClearColor(0xffffff, 0)

		camera.aspect = canvas.width / canvas.height;
		camera.updateProjectionMatrix();
	}
}