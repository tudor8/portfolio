# Portfolio

Repository for my Portfolio.

Visit at http://www.tudorgheorghe.net

# Getting Started

* This is a simple portfolio that uses ThreeJS to render some of my other projects in the background. The island and trees are loaded behind a loading screen. 
* The UI was done in simple html/css/javascript.

# Running

Simple way to host:

* using the console navigate to the folder

* python -m http.server

* visit 127.0.0.1:8000

ThreeJs doesn't work unless it is hosted by a server.

# Built With

* [ThreeJs](https://threejs.org/) - Used to render the models

* [jQuery](https://jquery.com/) - Web framework

* [HTML/CSS/Javascript]

* [JSON] - For loading data

# Authors

* **Tudor Gheorghe** - (http://www.tudorgheorghe.net)